CREATE TABLE Customer(
    id int(11) UNSIGNED UNIQUE NOT NULL,
    name varchar(40) NOT NULL,
    PRIMARY KEY id
)