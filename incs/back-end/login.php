<?php    
    $data = array();

    if(isset($_POST['email']) && isset($_POST['password'])) {

        if(!empty($_POST['email']) && !empty($_POST['password'])) {

            include('repositories/customer.php');
        
            $customer = getCustomer($_POST['email']);

            if($customer !== false && password_verify($_POST['password'], $customer['hash'])) {

                $_SESSION['user'] = [
                    'id' => $customer['id'],
                    'first_name' => $customer['first_name'],
                ];

                header('location: /?page=home');
                exit();

            } else {
                
                $data['error'] = "Incorrect credentials.";
                $data['email'] = $_POST['email'];
            }

        } else {

            $data['error'] = "Please fill in all your credentials.";
        }
    }
?>