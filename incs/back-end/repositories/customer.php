<?php

/**
 * Searches and retrieves a user by it's email address.
 *
 * @param string $email
 * @return array
 */
function getCustomer($email) {

    $conn = dbConnect();

    $statement = mysqli_prepare($conn, "SELECT id, first_name, hash FROM customer WHERE email=?");
    
    mysqli_stmt_bind_param($statement, 's', $email);
    mysqli_stmt_execute($statement);
    
    $result = mysqli_stmt_get_result($statement);

    if(mysqli_num_rows($result) > 0) {

        $customer = mysqli_fetch_assoc($result);
        return $customer;
    }

    return false;
}