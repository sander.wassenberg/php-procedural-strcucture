<?php

/**
 * Session releted setup
 */
session_start();
session_regenerate_id();

/**
 * Database
 *
 * @return mysqli
 */
function dbConnect() {

    $host = '127.0.0.1';
    $username = 'root';
    $password = '';
    $database = 'webshop';

    $conn = mysqli_connect($host, $username, $password, $database);
    
    if($conn) {
        return $conn;
    }

    die(mysqli_connect_error());
}
