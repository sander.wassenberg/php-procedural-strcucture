<?php include(__DIR__ . '/../back-end/login.php'); ?>
<?php include('layouts/head.php') ?>

<main class="login-page">
    <div class="container">
        <div class="card">
            <div class="card-header">
                inloggen
            </div>
            <div class="card-body">
                <form method="post">
                    <div class="form-group">
                        <label for="email">email</label>
                        <input type="email" id="email" <?php if(isset($data['email'])): ?> value="<?=$data['email']?>" <?php endif ?> name="email" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="password">wachtwoord</label>
                        <input type="password" id="password" name="password" class="form-control">
                    </div>
                    <?php if(isset($data['error'])): ?>
                        <div class="alert alert-danger">
                            <?=$data['error']?>
                        </div>
                    <?php endif ?>
                    <button type="submit" class="btn btn-primary">login</button>
                </form>
            </div>
        </div>
    </div>
</main>

<?php include('layouts/foot.php') ?>