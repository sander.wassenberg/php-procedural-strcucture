<?php

if(!isset($_GET['page'])) {
    header('location: /?page=home');
    exit();
}

switch($_GET['page']) {

    case "home":
        include('front-end/home.php');
    break;

    case "login":
        include('front-end/login.php');
    break;
}